Source: kdepim-runtime
Section: x11
Priority: optional
Maintainer: Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Sandro Knauß <hefee@debian.org>, Patrick Franz <deltaone@debian.org>,
Build-Depends: debhelper-compat (= 13),
               dh-sequence-kf6,
               cmake (>= 3.16~),
               extra-cmake-modules (>= 6.6.0~),
               gettext,
               libakonadi-dev (>= 4:24.12.2~),
               libakonadicalendar-dev (>= 4:24.12.2~),
               libakonadicontact-dev (>= 4:24.12.2~),
               libakonadimime-dev (>= 4:24.12.2~),
               libboost-system-dev,
               libboost-thread-dev,
               libkcalendarutils-dev (>= 4:24.12.2~),
               libkf6calendarcore-dev (>= 6.6.0~),
               libkf6codecs-dev (>= 6.6.0~),
               libkf6config-dev (>= 6.6.0~),
               libkf6configwidgets-dev (>= 6.6.0~),
               libkf6contacts-dev (>= 6.6.0~),
               libkf6dav-dev (>= 6.6.0~),
               libkf6itemmodels-dev (>= 6.6.0~),
               libkf6kcmutils-dev (>= 6.6.0~),
               libkf6kio-dev (>= 6.6.0~),
               libkf6notifications-dev (>= 6.6.0~),
               libkf6notifyconfig-dev (>= 6.6.0~),
               libkf6textwidgets-dev (>= 6.3.0~),
               libkf6wallet-dev (>= 6.6.0~),
               libkf6windowsystem-dev (>= 6.6.0~),
               libkgapi-dev (>= 24.12.2~),
               libkidentitymanagement-dev (>= 24.12.2~),
               libkimap-dev (>= 24.12.2~),
               libkldap-dev (>= 24.12.2~),
               libkmailtransport-dev (>= 24.12.2~),
               libkmbox-dev (>= 24.12.2~),
               libkmime-dev (>= 24.12.2~),
               libkolabxml-dev (>= 1.1~),
               libktextaddons-dev (>= 1.5.4~),
               libpimcommon-dev (>= 4:24.12.2~),
               libplasmaactivities-dev (>= 6.1.0~),
               libqca-qt6-dev (>= 2.3.7~),
               libsasl2-dev,
               pkgconf,
               qt6-base-dev (>= 6.7.0~),
               qt6-networkauth-dev (>= 6.7.0~),
               qt6-speech-dev (>= 6.7.0~),
               qt6-webengine-dev (>= 6.7.0~) [amd64 arm64 armhf i386],
               qtkeychain-qt6-dev (>= 0.14.2~),
               shared-mime-info (>= 1.8~),
               xsltproc,
Standards-Version: 4.7.0
Homepage: https://invent.kde.org/pim/kdepim-runtime
Vcs-Browser: https://salsa.debian.org/qt-kde-team/kde/kdepim-runtime
Vcs-Git: https://salsa.debian.org/qt-kde-team/kde/kdepim-runtime.git
Rules-Requires-Root: no

Package: kdepim-runtime
Architecture: amd64 arm64 armhf i386
Depends: akonadi-server,
         kio-ldap,
         kmailtransport-plugins,
         libsasl2-modules,
         libsasl2-modules-kdexoauth2,
         ${misc:Depends},
         ${shlibs:Depends},
Description: runtime components for Akonadi KDE
 This package contains Akonadi agents written using KDE Development Platform
 libraries.
 Any package that uses Akonadi should probably pull this in as a dependency.
 The kres-bridges is also parts of this package.
 .
 This package is part of the kdepim-runtime module.
